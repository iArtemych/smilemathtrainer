//
//  UIScrollView + Extension.swift
//  SmileMathTrainer
//
//  Created by Artem Chursin on 17.04.2020.
//  Copyright © 2020 Artem Chursin. All rights reserved.
//

import UIKit

extension UIScrollView {
    
    convenience init(viewBounds: CGRect, contentSize: CGSize) {
        self.init(frame: .zero)
        
        self.backgroundColor = .clear
        self.frame = viewBounds
        self.contentSize = contentSize
    }
}
