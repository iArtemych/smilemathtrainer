import UIKit

extension UIStackView {
    
    convenience init(axis: NSLayoutConstraint.Axis, spacing: CGFloat, backgroundColor: UIColor = .clear) {
        
        self.init()
        
        self.axis = axis
        self.spacing = spacing
        self.backgroundColor = backgroundColor
    }
}
