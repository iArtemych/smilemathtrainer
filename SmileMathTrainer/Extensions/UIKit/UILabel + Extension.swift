import UIKit

extension UILabel {
    convenience init(text: String, font: UIFont? = .systemFont(ofSize: 20), numberOfLines: Int = 0, textAlignment: NSTextAlignment = .center) {
        self.init()
        
        self.text = text
        self.font = font
        self.numberOfLines = numberOfLines
        self.textAlignment = textAlignment
        
        self.textColor = .systemBackground

    }
}
