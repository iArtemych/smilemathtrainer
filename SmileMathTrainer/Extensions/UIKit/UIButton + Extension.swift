import UIKit
extension UIButton {
    convenience init(systemName: String) {
        self.init(type: .system)
        
        self.backgroundColor = .clear
        self.titleLabel?.font = UIFont.systemFont(ofSize: 30, weight: .bold)
        self.setTitle(nil, for: .normal)
        
        let image = UIImage(systemName: systemName)
        self.setImage(image, for: .normal)
    }
}
