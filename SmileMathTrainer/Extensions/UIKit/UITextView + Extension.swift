import UIKit

extension UITextView {
    convenience init(text: String, font: UIFont? = .systemFont(ofSize: 20, weight: .medium)) {
        
        self.init()
        
        self.text = "Error"
        self.isEditable = false
        self.backgroundColor = .clear
        
        self.tintColor = .systemBackground

        self.font = font
    }
}
