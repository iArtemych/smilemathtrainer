import UIKit

class BackgroundBlureView: UIView {
    
    //MARK: - Variables
    lazy var backgroundImageView: UIImageView = {
         let imageView = UIImageView()
         imageView.translatesAutoresizingMaskIntoConstraints = false
         imageView.contentMode = .scaleAspectFill
         return imageView
     }()
    
    lazy var blurView: UIVisualEffectView = {
        
        let view = UIVisualEffectView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var vibrancyView: UIVisualEffectView = {
        
        let view = UIVisualEffectView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var effect: UIBlurEffect?
    
    //MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)

        setupView()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)

        setupView()
    }
    
    //MARK: - Methods
    func addVibrancy() {
        
        let vibrancyEffect: UIVibrancyEffect
        guard let blureEffect = effect else {
            return
        }
        
        vibrancyEffect = UIVibrancyEffect(blurEffect: blureEffect, style: .secondaryLabel)

        
        vibrancyView.effect = vibrancyEffect
        
        blurView.contentView.addSubview(vibrancyView)
        
        vibrancyView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        vibrancyView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        vibrancyView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        vibrancyView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
    }
    
    func addBackgroundImage(imageName: String) {
        
        backgroundImageView.image = UIImage(named: imageName)
        
        self.backgroundColor = .systemBackground
        self.backgroundImageView.tintColor = .systemBlue

        self.addSubview(backgroundImageView)
        self.sendSubviewToBack(backgroundImageView)
        backgroundImageView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        backgroundImageView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        backgroundImageView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        backgroundImageView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
    }
    
    func removeElements() {
        backgroundImageView.removeFromSuperview()
        blurView.removeFromSuperview()
        backgroundColor = .clear
    }
    
    //MARK: - Private methods
    private func setupView() {
        
        self.backgroundColor = .clear
        
        let blurEffect: UIBlurEffect
        
        blurEffect = UIBlurEffect(style: .systemChromeMaterial)
        
        effect = blurEffect
        
        self.addSubview(blurView)
        blurView.effect = blurEffect
        blurView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        blurView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        blurView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        blurView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
    }
    
}
