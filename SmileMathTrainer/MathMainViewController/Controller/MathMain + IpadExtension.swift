import Foundation
import UIKit

extension MathMainViewController {
    
    func setupIpadViews() {
        
        
        
        view.addSubview(iPadTopStackView)
        iPadTopStackView.addArrangedSubview(taskView)
        iPadTopStackView.addArrangedSubview(keyboardView)
        iPadTopStackView.distribution = .fillEqually
 
        NSLayoutConstraint.activate([
            iPadTopStackView.topAnchor.constraint(equalTo: view.topAnchor, constant: 25),
            iPadTopStackView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 30),
            iPadTopStackView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -30),
        ])

        iPadBottomStackView.distribution = .fillEqually
        view.addSubview(iPadBottomStackView)
        iPadBottomStackView.addArrangedSubview(paintView)
        iPadBottomStackView.addArrangedSubview(statisticView)

        NSLayoutConstraint.activate([
            iPadBottomStackView.topAnchor.constraint(equalTo: iPadTopStackView.bottomAnchor, constant: 30),
            iPadBottomStackView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 30),
            iPadBottomStackView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -30),
            iPadBottomStackView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -20),
            iPadBottomStackView.heightAnchor.constraint(equalTo: iPadTopStackView.heightAnchor)
        ])
    }
}
