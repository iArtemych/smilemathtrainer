import UIKit

class MathMainViewController: UIViewController {
    
    //MARK: - Constants
    
    //------------------TEST-----------------------------
    let names: [String] = ["Задача","Пример","График"]
    let tasksForStudent: [String] = ["Задача","Пример","График"]
    //-----------------------------------------------
    
    //MARK: - Variables
    lazy var backgroundView: BackgroundBlureView = {
        
        let view = BackgroundBlureView()
        view.addBackgroundImage(imageName: "Background")
        return view
    }()
    
    lazy var iphoneScrollView: UIScrollView = {

        let scroll = UIScrollView()
        scroll.backgroundColor = .clear
        return scroll
    }()
    let iphoneContentView = UIView()
    
    lazy var taskView = TaskView()
    lazy var keyboardView = KeyboardNumView()
    lazy var paintView = PaintView()
    lazy var statisticView = StatisticView()
    
    lazy var paintButton = CircleButton()
    lazy var statisticButton = CircleButton()
    
    lazy var iPadTopStackView = UIStackView(axis: .horizontal, spacing: 30)
    lazy var iPadBottomStackView = UIStackView(axis: .horizontal, spacing: 30)
    
    
    var taskMark: TaskMark = .notDone
    var answers: [Int] = [0,0,0]
    var taskNumber = 0
    
    
    //MARK: - LifeStyle ViewController
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        showDataOnViews()
        taskView.delegate = self
        paintView.delegate = self
        statisticView.delegate = self
        
    }
    
    //MARK: - Private methods
    private func tamicOff() {
        
        backgroundView.translatesAutoresizingMaskIntoConstraints = false
        
        iphoneContentView.translatesAutoresizingMaskIntoConstraints = false
        iphoneScrollView.translatesAutoresizingMaskIntoConstraints = false
        
        paintView.translatesAutoresizingMaskIntoConstraints = false
        keyboardView.translatesAutoresizingMaskIntoConstraints = false
        taskView.translatesAutoresizingMaskIntoConstraints = false
        statisticView.translatesAutoresizingMaskIntoConstraints = false
        
        iPadTopStackView.translatesAutoresizingMaskIntoConstraints = false
        iPadBottomStackView.translatesAutoresizingMaskIntoConstraints = false
    }
    
    private func setupView() {
        
        tamicOff()
        view.addSubview(backgroundView)
        view.sendSubviewToBack(backgroundView)
        setupBackground()
        
        switch UIDevice.current.userInterfaceIdiom {
        case .phone:
            setupIphoneViews()
        case .pad:
            setupIpadViews()
        default:
            fatalError("Unknown device")
        }
    }
    
    func setupBackground() {
        backgroundView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        backgroundView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        backgroundView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        backgroundView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
    }
    
    func showDataOnViews() {
        
        if taskNumber > names.count - 1 {taskNumber = 0}
        if taskNumber < 0 {taskNumber = names.count - 1}
        taskView.taskName = names[taskNumber]
        taskView.taskText = tasksForStudent[taskNumber]
        
        taskView.showData()
        
        let answer = answers[taskNumber]
        keyboardView.numPadView.answerString = String(answer)
        keyboardView.numPadView.showAnswer()
        
    }
}

