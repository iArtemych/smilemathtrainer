import UIKit

extension MathMainViewController {
    
    @objc func paintButtonTapped(){
        
        let paintVC = PaintViewController()
        paintVC.modalPresentationStyle = .fullScreen
        present(paintVC, animated: true, completion: nil)
    }
    
    @objc func statisticButtonTapped(){
        
        dismiss(animated: true, completion: nil)

    }
    
    func setupIphoneViews() {
        
        setupScroll()
        setupIphoneTask()
        setupIphoneKeyboard()
        setupEmptyView()
        setupPaintButton()
        setupStatisticButton()
        
    }
    
    private func setupScroll() {
        
        view.addSubview(iphoneScrollView)
        iphoneScrollView.addSubview(iphoneContentView)
        
        NSLayoutConstraint.activate([
            iphoneScrollView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            iphoneScrollView.widthAnchor.constraint(equalTo: view.widthAnchor),
            iphoneScrollView.topAnchor.constraint(equalTo: view.topAnchor),
            iphoneScrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
            
        ])
        
        NSLayoutConstraint.activate([
            iphoneContentView.centerXAnchor.constraint(equalTo: iphoneScrollView.centerXAnchor),
            iphoneContentView.widthAnchor.constraint(equalTo: iphoneScrollView.widthAnchor),
            iphoneContentView.topAnchor.constraint(equalTo: iphoneScrollView.topAnchor),
            iphoneContentView.bottomAnchor.constraint(equalTo: iphoneScrollView.bottomAnchor)
        ])
    }
    
    func setupEmptyView() {
        
        let emptyView = UIView()
        
        iphoneContentView.addSubview(emptyView)
        emptyView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            emptyView.centerXAnchor.constraint(equalTo: iphoneContentView.centerXAnchor),
            emptyView.topAnchor.constraint(equalTo: keyboardView.bottomAnchor, constant: 25),
            emptyView.widthAnchor.constraint(equalTo: iphoneContentView.widthAnchor),
            emptyView.bottomAnchor.constraint(equalTo: iphoneContentView.bottomAnchor),
            emptyView.heightAnchor.constraint(equalToConstant: view.frame.height / 20)
        ])
    }
    
    private func setupIphoneTask() {
        
        iphoneContentView.addSubview(taskView)
        
        NSLayoutConstraint.activate([
            taskView.centerXAnchor.constraint(equalTo: iphoneContentView.centerXAnchor),
            taskView.topAnchor.constraint(equalTo: iphoneContentView.topAnchor, constant: 40),
            taskView.widthAnchor.constraint(equalTo: iphoneContentView.widthAnchor),
            taskView.heightAnchor.constraint(equalToConstant: 406)
        ])
    }
    
    private func setupIphoneKeyboard() {
        
        iphoneContentView.addSubview(keyboardView)
        
        NSLayoutConstraint.activate([
            keyboardView.centerXAnchor.constraint(equalTo: iphoneContentView.centerXAnchor),
            keyboardView.topAnchor.constraint(equalTo: taskView.bottomAnchor, constant: 25),
            keyboardView.widthAnchor.constraint(equalTo: iphoneContentView.widthAnchor),
            keyboardView.heightAnchor.constraint(equalToConstant: 353)
        ])
    }
    
    private func setupPaintButton() {
        
        view.addSubview(paintButton)
        view.bringSubviewToFront(paintButton)
        paintButton.translatesAutoresizingMaskIntoConstraints = false
        
        paintButton.setTitle(imageSystemName: "pencil.and.outline", imageName: "edit")
        
        NSLayoutConstraint.activate([
            paintButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -15),
            paintButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -20)
        ])
        
        paintButton.vibrancyButton.addTarget(self, action: #selector(paintButtonTapped), for: .touchUpInside)
    }
    
    private func setupStatisticButton() {
        
        view.addSubview(statisticButton)
        view.bringSubviewToFront(statisticButton)
        statisticButton.translatesAutoresizingMaskIntoConstraints = false
        
        statisticButton.setTitle(imageSystemName: "line.horizontal.3", imageName: "lines")
        
        NSLayoutConstraint.activate([
            statisticButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 15),
            statisticButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -20)
        ])
        
        statisticButton.vibrancyButton.addTarget(self, action: #selector(statisticButtonTapped), for: .touchUpInside)
    }
}
