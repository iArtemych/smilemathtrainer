import UIKit

extension MathMainViewController: StatisticViewDelegate {
    func toTasksListButtonTapped() {
        
        dismiss(animated: true, completion: nil)
    }
}

extension MathMainViewController: TaskViewDelegate {
    func didTapButton(taskmark: TaskMark?, taskNumberMark: TaskNumberMark?) {
        
        if let mark = taskmark {
            switch mark {
            case .notDone:
                taskMark = .notDone
            case .readyToСheck:
                taskMark = .readyToСheck
                print("ready")
            case .toDo:
                taskMark = .toDo
                print("toDo")
            }
        }
        
        if let mark = taskNumberMark {
            
            let strAnswer = Int(keyboardView.numPadView.answerString)
            
            guard let answer = strAnswer else {
                
                UIAlertController.showAlert(title: "Ошибка!",
                                            message: "Неверный формат ответа",
                                            inViewController: self)
                return
            }
            answers[taskNumber] = answer
            
            switch mark {
            case .nextTask:
                taskNumber += 1
            case .prevTask:
                taskNumber -= 1
            }
            
            showDataOnViews()
        }
    }
}

extension MathMainViewController: PaintViewIpadDelegate {
    
    func didTapButton(paintButton: PaintButton, shareImage: [UIImage]?) {
                
        switch paintButton {
        case .clearButtonTap:
            resetPaintTap()
        case .fullScreenButtonTap:
            self.fullScreenTap()
        }
    }
    
    func resetPaintTap() {
        
    }
    
    func fullScreenTap() {
        let paintVC = PaintViewController()
        paintVC.modalPresentationStyle = .fullScreen
        present(paintVC, animated: true, completion: nil)
    }
}
