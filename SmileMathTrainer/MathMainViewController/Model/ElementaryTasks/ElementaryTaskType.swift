import Foundation

enum ElementaryTaskType {
    case CleverGeneration
    case VietTheorem
    case GreatestCommonDivisor
    case SmallestCommonMultiple
}
