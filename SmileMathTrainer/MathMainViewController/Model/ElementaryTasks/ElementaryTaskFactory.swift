import Foundation

class ElementaryTaskFactory: TaskFactory {
    
    func produce(type: TaskType?) -> Task {
        
        return ElementaryTask(type: type)
    }
}
