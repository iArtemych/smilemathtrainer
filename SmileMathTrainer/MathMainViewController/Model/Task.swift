import UIKit

protocol Task {
    var answer: Int? { get }
    var taskText: String? { get }
    var taskImage: UIImage? { get set }
    var type: TaskType? {get set}
    var tasksNumbers: [Int]? { get set }
    func generateTask()
}

enum TaskType {
    case SimpleTextTask(type: SimpleTextTaskType)
    case ElementaryTask(type: ElementaryTaskType)
}
