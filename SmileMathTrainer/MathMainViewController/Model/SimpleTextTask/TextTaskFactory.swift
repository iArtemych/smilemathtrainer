import Foundation

class SimpleTextTaskFactory: TaskFactory {
    
    func produce(type: TaskType?) -> Task {
        
        return SimpleTextTask(type: type)
    }
}
