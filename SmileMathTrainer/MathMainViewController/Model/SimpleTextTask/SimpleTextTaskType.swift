import Foundation

enum SimpleTextTaskType {
    case CleverGeneration
    case CheckInStore
    case MilesInKilometers
    case ElectricityConsumption
    case QuantityOfGoodsForMoney
    case PercentRiseInPrice
    case SalesForCompany
}
