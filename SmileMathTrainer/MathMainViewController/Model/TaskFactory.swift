protocol TaskFactory {
    
    func produce(type: TaskType?) -> Task
}
