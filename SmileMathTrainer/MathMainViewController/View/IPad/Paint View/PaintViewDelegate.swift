import UIKit

protocol PaintViewIpadDelegate: class {

    func didTapButton(paintButton: PaintButton, shareImage: [UIImage]?)
}
