import UIKit

enum PaintButton {
    case clearButtonTap
    case fullScreenButtonTap
}

class PaintView: UIView {
    //MARK: - Constants
    
    //MARK: - Variables
    weak var delegate: PaintViewIpadDelegate!
    
    lazy var backgroundView: BackgroundBlureView = {
        
        let view = BackgroundBlureView()
        view.addVibrancy()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var paintImage: UIImageView = {
       
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.backgroundColor = .clear
        return image
    }()

    lazy var resetButton = UIButton(systemName: "trash")
    lazy var fullScreenButton = UIButton(systemName: "arrow.up.left.and.arrow.down.right")
    
    private var lastPoint = CGPoint.zero
    private var swiped = false
    
    //MARK: - LifeStyle ViewController
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.backgroundColor = .clear
        self.layer.masksToBounds = true
        self.layer.cornerRadius = self.bounds.width / 7
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        setupView()
    }
    
    //MARK: - Actions
    @objc func pressedReset(sender: UIButton!){
         
        sender.isSelected = true
        
        delegate.didTapButton(paintButton: .clearButtonTap, shareImage: nil)
        paintImage.image = nil
    }
    
    //MARK: - Navigation
    @objc func pressedFull(sender: UIButton!){
        
        delegate.didTapButton(paintButton: .fullScreenButtonTap, shareImage: nil)
    }
    
    //MARK: - Private methods
    private func setupImage() {
        
        backgroundView.vibrancyView.contentView.addSubview(paintImage)
        
        paintImage.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        paintImage.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        paintImage.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        paintImage.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
    }
    
    //-------------------------------------------------
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        swiped = false
        if let touch = touches.first {
            lastPoint = touch.location(in: self)
        }
    }
    
    func drawLineFrom(fromPoint: CGPoint, toPoint: CGPoint) {
      
      // 1
        UIGraphicsBeginImageContext(self.frame.size)
        
        guard let context = UIGraphicsGetCurrentContext() else {
            return
        }
        paintImage.image?.draw(in: CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height))
      
        let startPoint = CGPoint(x: fromPoint.x, y: fromPoint.y)
        let endPoint = CGPoint(x: toPoint.x, y: toPoint.y)
        
        context.move(to: startPoint)
        context.addLine(to: endPoint)
        
        context.setLineCap(CGLineCap.round)
        context.setLineWidth(3.0)
        context.setStrokeColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
        context.setBlendMode(CGBlendMode.normal)
        
        context.strokePath()
        
        paintImage.image = UIGraphicsGetImageFromCurrentImageContext()
        paintImage.alpha = 1.0
        UIGraphicsEndImageContext()
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        swiped = true
        if let touch = touches.first {
            let currentPoint = touch.location(in: self)
            drawLineFrom(fromPoint: lastPoint, toPoint: currentPoint)
            
            lastPoint = currentPoint
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if !swiped {
            drawLineFrom(fromPoint: lastPoint, toPoint: lastPoint)
        }
        
        UIGraphicsBeginImageContext(paintImage.frame.size)
        paintImage.image?.draw(in: CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height), blendMode: CGBlendMode.normal, alpha: 1.0)
        paintImage.image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
    }
    //-------------------------------------------------
    
    private func setupView() {
        
        setupBackgroundView()
        setupImage()
        setupResetButton()
        setupFullScreenButton()
    }
    
    private func setupFullScreenButton() {
        fullScreenButton.translatesAutoresizingMaskIntoConstraints = false
        backgroundView.vibrancyView.contentView.addSubview(fullScreenButton)
        let verticalSpaceTop = NSLayoutConstraint(item: fullScreenButton, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 20)
        let horisontalSpaceTrailing = NSLayoutConstraint(item: fullScreenButton, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: -50)
        NSLayoutConstraint.activate([verticalSpaceTop, horisontalSpaceTrailing])
        fullScreenButton.addTarget(self, action: #selector(self.pressedFull(sender:)), for: .touchUpInside)
    }
    
    private func setupResetButton() {
        
        resetButton.translatesAutoresizingMaskIntoConstraints = false
        backgroundView.vibrancyView.contentView.addSubview(resetButton)
        let verticalSpaceTop = NSLayoutConstraint(item: resetButton, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 20)
        let horisontalSpaceLeading = NSLayoutConstraint(item: resetButton, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 50)
        NSLayoutConstraint.activate([verticalSpaceTop, horisontalSpaceLeading])

        resetButton.addTarget(self, action: #selector(self.pressedReset(sender:)), for: .touchUpInside)
    }

    private func setupBackgroundView() {
        
        self.addSubview(backgroundView)
        backgroundView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        backgroundView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        backgroundView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        backgroundView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
    }
}

