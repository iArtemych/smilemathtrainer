import UIKit

protocol CircleButtonDelegate: class {
    
    func didTapButton(button: CircleButtonType)
}

enum CircleButtonType {
    
    case prefference
    case paint
}

