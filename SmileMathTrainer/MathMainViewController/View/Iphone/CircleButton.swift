import UIKit

class CircleButton: UIView {
    
    //MARK: - Constants
    
    //MARK: - Variables
    lazy var backgroundView: BackgroundBlureView = {
        
        let view = BackgroundBlureView()
        view.addVibrancy()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    var vibrancyButton = UIButton(type: .system)
    private var shadowLayer: CAShapeLayer!
    
    //MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)

        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        setupView()
    }
    
    //MARK: - Methods
    override func layoutSubviews() {
        
        let radius = self.frame.height / 2
        self.layer.masksToBounds = true
        self.layer.cornerRadius = radius
        
    }
    
    func setTitle(imageSystemName: String, imageName: String) {
        if #available(iOS 13.0, *) {
            vibrancyButton.setTitle(nil, for: .normal)
            vibrancyButton.setImage(UIImage(systemName: imageSystemName), for: .normal)
        } else {
            if let image = UIImage(named: imageName) {
                vibrancyButton.setTitle(nil, for: .normal)
                vibrancyButton.setImage(image, for: .normal)
            } else {
                vibrancyButton.setTitle(imageName, for: .normal)
            }
        }
    }
    
    //MARK: - Private methods
    private func setupView() {
        
        self.backgroundColor = .clear
        
        let viewSize: CGFloat = 60
        self.widthAnchor.constraint(equalToConstant: viewSize).isActive = true;
        self.heightAnchor.constraint(equalToConstant: viewSize).isActive = true;
        
        self.addSubview(backgroundView)
        backgroundView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        backgroundView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        backgroundView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        backgroundView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        
        setupButton()
    }
    
    private func setupButton() {
        
        backgroundView.vibrancyView.contentView.addSubview(vibrancyButton)
        vibrancyButton.translatesAutoresizingMaskIntoConstraints = false
        vibrancyButton.topAnchor.constraint(equalTo: backgroundView.topAnchor).isActive = true
        vibrancyButton.bottomAnchor.constraint(equalTo: backgroundView.bottomAnchor).isActive = true
        vibrancyButton.leadingAnchor.constraint(equalTo: backgroundView.leadingAnchor).isActive = true
        vibrancyButton.trailingAnchor.constraint(equalTo: backgroundView.trailingAnchor).isActive = true
    }
}
