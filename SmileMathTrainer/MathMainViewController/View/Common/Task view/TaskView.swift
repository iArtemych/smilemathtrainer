import UIKit

class TaskView: UIView {
    
    //MARK: - Variables
    var taskName = "Error"
    var taskText = "Error"
    var taskMark = TaskMark.notDone
    
    weak var delegate: TaskViewDelegate!
    
    lazy var backgroundView = BackgroundBlureView()
    lazy var titleLabel = UILabel(text: "Error", font: .systemFont(ofSize: 35, weight: .medium))
    lazy var taskTextView = UITextView(text: "Error")
    lazy var vibrancyBackButton: UIButton = UIButton(systemName: "lessthan.circle")
    lazy var vibrancyNextButton: UIButton = UIButton(systemName: "greaterthan.circle")
    lazy var vibrancyToDoButton: UIButton = UIButton(systemName: "exclamationmark.triangle")
    lazy var vibrancyCheckRedyButton: UIButton = UIButton(systemName: "checkmark.seal")
    
    //MARK: - LifeStyle View
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.backgroundColor = .clear
        self.layer.masksToBounds = true
        self.layer.cornerRadius = self.bounds.width / 7
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        setupView()
    }
    
    //MARK: - Actions
    @objc func pressedBack(sender: UIButton!) {
        
        delegate.didTapButton(taskmark: nil, taskNumberMark: .prevTask)
    }
    
    @objc func pressedNext(sender: UIButton!){
        delegate.didTapButton(taskmark: nil, taskNumberMark: .nextTask)
    }
    
    @objc func pressedToDo(sender: UIButton!){
        taskMark = .toDo
        setupBorderMark()
        
        delegate.didTapButton(taskmark: nil, taskNumberMark: nil)
    }
    
    @objc func pressedReady(sender: UIButton!){
        taskMark = .readyToСheck
        setupBorderMark()
        
        delegate.didTapButton(taskmark: nil, taskNumberMark: nil)
    }
    
    //MARK: - Methods
    func showData(){
        
        titleLabel.text = taskName
        taskTextView.text = taskText
    }
    
    func setupBorderMark() {
        
        switch taskMark {
        case .notDone:
            vibrancyToDoButton.layer.borderWidth = 0
            vibrancyCheckRedyButton.layer.borderWidth = 0
        case .readyToСheck:
            vibrancyToDoButton.layer.borderWidth = 0
            vibrancyCheckRedyButton.layer.borderWidth = 1
        case .toDo:
            vibrancyToDoButton.layer.borderWidth = 1
            vibrancyCheckRedyButton.layer.borderWidth = 0
        }
    }
    
    //MARK: - Private methods
    private func setupView() {
        backgroundView.addVibrancy()
        self.addSubview(backgroundView)
        backgroundView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        backgroundView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        backgroundView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        backgroundView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        
        tamicOff()
        configTaskLable()
        setupTaskText()
        setupBackButton()
        setupNextButton()
        setupToDoButton()
        setupReadyButton()
    }
    
    private func tamicOff() {
        
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        taskTextView.translatesAutoresizingMaskIntoConstraints = false
        vibrancyBackButton.translatesAutoresizingMaskIntoConstraints = false
        vibrancyNextButton.translatesAutoresizingMaskIntoConstraints = false
        vibrancyToDoButton.translatesAutoresizingMaskIntoConstraints = false
        vibrancyCheckRedyButton.translatesAutoresizingMaskIntoConstraints = false
        backgroundView.translatesAutoresizingMaskIntoConstraints = false
    }
    
    private func configTaskLable() {
        backgroundView.vibrancyView.contentView.addSubview(titleLabel)
        
        let verticalSpaceTop = NSLayoutConstraint(item: titleLabel, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 20)
        let horisontalSpaceTrailing = NSLayoutConstraint(item: titleLabel, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: -25)
        let horisontalSpaceLeading = NSLayoutConstraint(item: titleLabel, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 25)
        NSLayoutConstraint.activate([verticalSpaceTop, horisontalSpaceLeading, horisontalSpaceTrailing])
    }
    
    private func setupTaskText() {
        backgroundView.vibrancyView.contentView.addSubview(taskTextView)

        let verticalSpaceTop = NSLayoutConstraint(item: taskTextView, attribute: .top, relatedBy: .equal, toItem: titleLabel, attribute: .bottom, multiplier: 1, constant: 5)
        let verticalSpaceBottom = NSLayoutConstraint(item: taskTextView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: -55)
        let horisontalSpaceTrailing = NSLayoutConstraint(item: taskTextView, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: -20)
        let horisontalSpaceLeading = NSLayoutConstraint(item: taskTextView, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 20)
        NSLayoutConstraint.activate([verticalSpaceTop, verticalSpaceBottom, horisontalSpaceLeading, horisontalSpaceTrailing])
        
    }
    
    private func setupBackButton(){
        
        backgroundView.vibrancyView.contentView.addSubview(vibrancyBackButton)
        
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: vibrancyBackButton, attribute: .top, relatedBy: .equal, toItem: taskTextView, attribute: .bottom, multiplier: 1, constant: 5),
            NSLayoutConstraint(item: vibrancyBackButton, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: -15),
            NSLayoutConstraint(item: vibrancyBackButton, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 50)
        ])
        
        vibrancyBackButton.addTarget(self, action: #selector(self.pressedBack(sender:)), for: .touchUpInside)
    }
    
    private func setupNextButton(){

        backgroundView.vibrancyView.contentView.addSubview(vibrancyNextButton)

        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: vibrancyNextButton, attribute: .top, relatedBy: .equal, toItem: taskTextView, attribute: .bottom, multiplier: 1, constant: 5),
            NSLayoutConstraint(item: vibrancyNextButton, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: -15),
            NSLayoutConstraint(item: vibrancyNextButton, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: -50)
        ])
        vibrancyNextButton.addTarget(self, action: #selector(self.pressedNext(sender:)), for: .touchUpInside)
    }
    
    private func setupToDoButton(){

        backgroundView.vibrancyView.contentView.addSubview(vibrancyToDoButton)
        
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: vibrancyToDoButton, attribute: .top, relatedBy: .equal, toItem: taskTextView, attribute: .bottom, multiplier: 1, constant: 5),
            NSLayoutConstraint(item: vibrancyToDoButton, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: -15),
            vibrancyToDoButton.trailingAnchor.constraint(equalTo: self.centerXAnchor, constant: -25)
        ])
        
        vibrancyToDoButton.addTarget(self, action: #selector(self.pressedToDo(sender:)), for: .touchUpInside)
    }
    
    private func setupReadyButton(){

        backgroundView.vibrancyView.contentView.addSubview(vibrancyCheckRedyButton)
        
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: vibrancyCheckRedyButton, attribute: .top, relatedBy: .equal, toItem: taskTextView, attribute: .bottom, multiplier: 1, constant: 5),
            NSLayoutConstraint(item: vibrancyCheckRedyButton, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: -15),
            vibrancyCheckRedyButton.leadingAnchor.constraint(equalTo: self.centerXAnchor, constant: 25)
        ])

        vibrancyCheckRedyButton.addTarget(self, action: #selector(self.pressedReady(sender:)), for: .touchUpInside)
    }
}
