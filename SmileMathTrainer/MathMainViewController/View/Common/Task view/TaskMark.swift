import Foundation

enum TaskMark {
    
    case notDone
    case toDo
    case readyToСheck
}

enum TaskNumberMark {
    
    case prevTask
    case nextTask
}
