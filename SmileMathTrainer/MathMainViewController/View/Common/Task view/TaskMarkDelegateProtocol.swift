import Foundation

protocol TaskViewiPadDelegate: class {

    func didTapButton(taskmark: TaskMark?, taskNumberMark: TaskNumberMark?)
}

protocol TaskViewDelegate: class {

    func didTapButton(taskmark: TaskMark?, taskNumberMark: TaskNumberMark?)
}
