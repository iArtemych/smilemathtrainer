import UIKit

class KeyboardNumView: UIView {

    //MARK: - Constants
    
    //MARK: - Variables
    var numPadView: NumPadView!
    
    lazy var backgroundView: BackgroundBlureView = {
        
        let view = BackgroundBlureView()
        view.addVibrancy()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    //MARK: - LifeStyle View
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.backgroundColor = .clear
        self.layer.masksToBounds = true
        self.layer.cornerRadius = self.bounds.width / 7
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        setupView()
    }
    
    //MARK: - Actions
    
    //MARK: - Navigation
    
    //MARK: - Private methods
    private func setupView() {
        
        self.addSubview(backgroundView)
        backgroundView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        backgroundView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        backgroundView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        backgroundView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        setupKeyboard()
    }
    
    private func setupKeyboard() {
        
        numPadView = NumPadView.loadFromNib()
        backgroundView.vibrancyView.contentView.addSubview(numPadView)
        numPadView.translatesAutoresizingMaskIntoConstraints = false
        
        if let pad = numPadView {
            let verticalSpaceTop = NSLayoutConstraint(item: pad, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0)
            let verticalSpaceBottom = NSLayoutConstraint(item: pad, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: -5)
            let horisontalSpaceTrailing = NSLayoutConstraint(item: pad, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: -20)
            let horisontalSpaceLeading = NSLayoutConstraint(item: pad, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 20)
            NSLayoutConstraint.activate([verticalSpaceTop, verticalSpaceBottom, horisontalSpaceLeading, horisontalSpaceTrailing])
        }
    }
}
