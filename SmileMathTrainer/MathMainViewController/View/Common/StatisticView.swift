import UIKit

class StatisticView: UIView {

    //MARK: - Constants
    
    //MARK: - Variables
    lazy var backgroundView: BackgroundBlureView = {
        
        let view = BackgroundBlureView()
        view.addVibrancy()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var taskButton = UIButton(systemName: "line.horizontal.3")
    weak var delegate: StatisticViewDelegate?
    //MARK: - Init
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.backgroundColor = .clear
        self.layer.masksToBounds = true
        self.layer.cornerRadius = self.bounds.width / 7
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
        setupBackButton()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        setupView()
        setupBackButton()
    }
    
    //MARK: - Actions
    @objc private func tasksListButtonTapped() {
        delegate?.toTasksListButtonTapped()
    }
    //MARK: - Navigation
    
    //MARK: - Private methods
    private func setupView() {
        
        self.addSubview(backgroundView)
        backgroundView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        backgroundView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        backgroundView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        backgroundView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
    }
    
    private func setupBackButton() {
        
        taskButton.translatesAutoresizingMaskIntoConstraints = false
        backgroundView.vibrancyView.contentView.addSubview(taskButton)
        NSLayoutConstraint.activate([
            taskButton.centerXAnchor.constraint(equalTo: backgroundView.centerXAnchor),
            taskButton.centerYAnchor.constraint(equalTo: backgroundView.centerYAnchor)
        ])
        taskButton.addTarget(self, action: #selector(tasksListButtonTapped), for: .touchUpInside)
    }
}

protocol StatisticViewDelegate: class {
    func toTasksListButtonTapped()
}
