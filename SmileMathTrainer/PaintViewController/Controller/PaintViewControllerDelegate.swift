import UIKit

protocol PaintViewControllerDelegate: class {

    func didTapButton(paintButton: PaintFullButtons, shareImage: [UIImage]?)
}
