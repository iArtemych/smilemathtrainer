import UIKit

enum PaintFullButtons {
    case close
    case share
}

class PaintViewController: UIViewController, HasCustomView {
    typealias CustomView = PaintFullView
    
    
    //MARK: - Constants
    
    //MARK: - Variables
    
    //MARK: - Outlets
    
    //MARK: - LifeStyle ViewController
    override func viewWillTransition( to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator ) {
//            print( "h:\(self.view.bounds.size.height)" )
//            print( "w:\(self.view.frame.size.width)" )
    }
    
    override func loadView() {
        let customView = CustomView()
        customView.delegate = self
        view = customView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        customView.iPadDelegate = self
        
    }
    
    override func viewWillLayoutSubviews() {
        
        customView.screenSize(width: self.view.frame.size.width, height: self.view.bounds.size.height)
    }
    
    //MARK: - Actions

    //MARK: - Navigation
    
    //MARK: - Private methods
    private func shareTap(images: [UIImage]?) {
        
        print("2")
        guard let image = images else {
            UIAlertController.showAlert(title: "Ошибка!",
                                        message: "Ошибка сохранения изображения",
                                        inViewController: self)
            
            return
        }
        print("3")
        
        
        let activity = UIActivityViewController(activityItems: image, applicationActivities: nil)
        activity.popoverPresentationController?.sourceView = self.view
        activity.popoverPresentationController?.sourceRect = CGRect(x: view.bounds.midX, y: view.bounds.midY, width: 0, height: 0)
        activity.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.init(rawValue: 0)
        self.present(activity, animated: true, completion: nil)
    }
}

extension PaintViewController: PaintViewControllerDelegate {
    func didTapButton(paintButton: PaintFullButtons, shareImage: [UIImage]?) {
        
        switch paintButton {
        case .close:
            print("123")
            dismiss(animated: true, completion: nil)
        case .share:
            print("VC Share")
            shareTap(images: shareImage)
        }
    }
}
