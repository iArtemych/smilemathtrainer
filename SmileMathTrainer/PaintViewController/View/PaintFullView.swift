import UIKit

class PaintFullView: UIView {

    //MARK: - Constants
    
    //MARK: - Variables
    var landscapeConstraints: [NSLayoutConstraint] = []
    var portretConstraints: [NSLayoutConstraint] = []
    
    var lastPoint = CGPoint.zero
    var red: CGFloat = 0.0
    var green: CGFloat = 0.0
    var blue: CGFloat = 0.0
    var brushWidth: CGFloat = 10.0
    var opacity: CGFloat = 1.0
    var swiped = false
    
    var paintViewWidth: CGFloat = 0
    var paintViewHeight: CGFloat = 0
    var f = 0
    
    weak var delegate: PaintViewControllerDelegate!
    
    lazy var closeVCButton = CircleButton()
    lazy var shareButton = CircleButton()
    
    lazy var paintImage: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        if #available(iOS 13.0, *) {
            imageView.backgroundColor = .systemBackground
//            let new = imageView.image?.newImageWithColor(tintColor: .red)
//            imageView.image = new
//            imageView.image?.imageWithColor(tintColor: .systemBackground, image: &imageView.image!)
        } else {
            imageView.backgroundColor = .black
        }
        return imageView
    }()
    
    //MARK: - Outlets
    
    //MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        setupView()
    }
    
    //MARK: - Actions
    @objc func pressedShare(sender: UIButton!){
        print("Share")
        
        guard let image = paintImage.image else{
            return
        }
        delegate.didTapButton(paintButton: .share, shareImage: [image])
    }
    
    @objc func pressedCloseButton(sender: UIButton!) {
        print("Close")

        delegate.didTapButton(paintButton: .close, shareImage: nil)
    }
    //MARK: - Navigation
    
    
    
    //MARK: - Private methods
    
    func screenSize(width: CGFloat, height: CGFloat) {
        
        var w = width
        var h = height
        
        if h > w {
            
            let temp = w
            w = h
            h = temp
        }
        
        paintViewWidth = w
        paintViewHeight = h
    
        if f == 0 {
            setupPaintView()
            f += 1
        }

        paintImagecalCulation()
    }
    
    private func setupPaintView() {
        
        paintImage.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        paintImage.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        
        
        landscapeConstraints  = [
        paintImage.heightAnchor.constraint(equalToConstant: paintViewHeight),
        paintImage.widthAnchor.constraint(equalToConstant: paintViewWidth)]

        portretConstraints = [
        paintImage.heightAnchor.constraint(equalToConstant: paintViewHeight / (paintViewWidth / paintViewHeight)),
        paintImage.widthAnchor.constraint(equalToConstant: paintViewHeight)]
//        dddd()
    }
    
    private func paintImagecalCulation() {
        
        if UIDevice.current.orientation == .landscapeLeft || UIDevice.current.orientation == .landscapeRight {
            NSLayoutConstraint.deactivate(portretConstraints)
            NSLayoutConstraint.activate(landscapeConstraints)
        } else if UIDevice.current.orientation == .portrait || UIDevice.current.orientation == .portraitUpsideDown {
            NSLayoutConstraint.deactivate(landscapeConstraints)
            NSLayoutConstraint.activate(portretConstraints)
        }
    }
    
    private func setupCloseButton() {
        
        setupButtons(button: closeVCButton, name: "minimize")
        let verticalSpaceTop = NSLayoutConstraint(item: closeVCButton, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 30)
        let horisontalSpaceLeading = NSLayoutConstraint(item: closeVCButton, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 20)
        NSLayoutConstraint.activate([verticalSpaceTop, horisontalSpaceLeading])
        closeVCButton.vibrancyButton.addTarget(self, action: #selector(self.pressedCloseButton(sender:)), for: .touchUpInside)
    }
    
    private func setupShareButton() {
        
        setupButtons(button: shareButton, name: "share")
        let verticalSpaceTop = NSLayoutConstraint(item: shareButton, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 30)
        let horisontalSpaceTrailing = NSLayoutConstraint(item: shareButton, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: -20)
        NSLayoutConstraint.activate([verticalSpaceTop, horisontalSpaceTrailing])
        shareButton.vibrancyButton.addTarget(self, action: #selector(self.pressedShare(sender:)), for: .touchUpInside)
    }
    
    private func setupButtons(button: CircleButton, name: String) {
        
        self.addSubview(button)
        self.bringSubviewToFront(button)
        button.translatesAutoresizingMaskIntoConstraints = false
        
        if let image = UIImage(named: name){
            button.vibrancyButton.setTitle(nil, for: .normal)
            button.vibrancyButton.setImage(image, for: .normal)
        } else {
            button.vibrancyButton.setTitle(name, for: .normal)
        }
    }
    
    private func setupView() {
        
        if #available(iOS 13.0, *) {
            self.backgroundColor = .systemGray5
        } else {
            self.backgroundColor = .gray
        }
        
        self.addSubview(paintImage)
        
        setupCloseButton()
        setupShareButton()
        
    }
    
    func dddd() {
        guard let currentCGImage = paintImage.image!.cgImage else { return }
        let currentCIImage = CIImage(cgImage: currentCGImage)

        let filter = CIFilter(name: "CIColorMonochrome")
        filter?.setValue(currentCIImage, forKey: "inputImage")

        // set a gray value for the tint color
        filter?.setValue(CIColor(color: UIColor.black), forKey: "inputColor")

        filter?.setValue(1.0, forKey: "inputIntensity")
        guard let outputImage = filter?.outputImage else { return }

        let context = CIContext()

        if let cgimg = context.createCGImage(outputImage, from: outputImage.extent) {
            let processedImage = UIImage(cgImage: cgimg)
            print(processedImage.size)
        }
    }
    
//    @objc func nons() {
//        let f = paintImage.image?.withRenderingMode(.alwaysOriginal)
//        paintImage.image = f
//        paintImage.tintColor = .black
//    }
    
    // MARK: - Drowing
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        swiped = false
        if let touch = touches.first {
            lastPoint = touch.location(in: paintImage)
            
            
            print("begin x \(lastPoint.x) y \(lastPoint.y)")
        }
    }
    
    func drawLineFrom(fromPoint: CGPoint, toPoint: CGPoint) {

      // 1
//        UIGraphicsBeginImageContext(paintImage.bounds.size)
        UIGraphicsBeginImageContext(CGSize(width: paintViewWidth, height: paintViewHeight))
        
        guard let context = UIGraphicsGetCurrentContext() else {
            return
        }
        paintImage.image?.draw(in: CGRect(x: 0, y: 0, width: paintViewWidth, height: paintViewHeight))
      
        let startPoint = CGPoint(x: fromPoint.x, y: fromPoint.y)
        let endPoint = CGPoint(x: toPoint.x, y: toPoint.y)
        print("----------------------------------------")
        print("x \(startPoint.x)  y \(startPoint.y)")
        print("x \(toPoint.x)  y \(toPoint.y)")
        
        context.move(to: startPoint)
        context.addLine(to: endPoint)
        
        context.setLineCap(CGLineCap.round)
        context.setLineWidth(3.0)
        context.setStrokeColor(red: 255.0, green: 255.0, blue: 255.0, alpha: 1.0)
//        context.setStrokeColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
        context.setBlendMode(CGBlendMode.normal)
        
        context.strokePath()
        
        paintImage.image = UIGraphicsGetImageFromCurrentImageContext()
        paintImage.alpha = 1.0
        UIGraphicsEndImageContext()
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        swiped = true
        if let touch = touches.first {
            let currentPoint = touch.location(in: paintImage)
            drawLineFrom(fromPoint: lastPoint, toPoint: currentPoint)
            
            lastPoint = currentPoint
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if !swiped {
            drawLineFrom(fromPoint: lastPoint, toPoint: lastPoint)
        }
        
//        UIGraphicsBeginImageContext(paintImage.bounds.size)
        UIGraphicsBeginImageContext(CGSize(width: paintViewWidth, height: paintViewHeight))
        paintImage.image?.draw(in: CGRect(x: 0, y: 0, width: paintViewWidth, height: paintViewHeight), blendMode: CGBlendMode.normal, alpha: 1.0)
        paintImage.image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
    }
}



extension UIImage {
    func newImageWithColor(tintColor: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        
        let context = UIGraphicsGetCurrentContext()!
        context.translateBy(x: 0, y: self.size.height)
        context.scaleBy(x: 1.0, y: -1.0);
        context.setBlendMode(.normal)
        
        let rect = CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height) as CGRect
        context.clip(to: rect, mask: self.cgImage!)
        tintColor.setFill()
        context.fill(rect)
        
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()!
        
        UIGraphicsEndImageContext()
        
        return newImage
    }
}

extension UIImage {
    func imageWithColor(tintColor: UIColor, image: inout UIImage) {
        UIGraphicsBeginImageContextWithOptions(image.size, false, image.scale)
        
        let context = UIGraphicsGetCurrentContext()!
        context.translateBy(x: 0, y: image.size.height)
        context.scaleBy(x: 1.0, y: -1.0);
        context.setBlendMode(.normal)
        
        let rect = CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height) as CGRect
        context.clip(to: rect, mask: image.cgImage!)
        tintColor.setFill()
        context.fill(rect)
        
        image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
    }
}
