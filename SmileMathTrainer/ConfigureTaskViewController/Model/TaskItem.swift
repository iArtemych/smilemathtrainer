//
//  TaskItem.swift
//  SmileMathTrainer
//
//  Created by Artem Chursin on 26.04.2020.
//  Copyright © 2020 Artem Chursin. All rights reserved.
//

import UIKit

enum Section {
    case main
}

class OutlineItem: Hashable {
    let title: String
    let titleImage: String
    let indentLevel: Int
    let subitems: [OutlineItem]
    let outlineViewController: UIViewController.Type?
    let tasgGroupKey: TaskEnum
    
    var isExpanded = false
    
    init(title: String,
         titleImage: String = "",
         indentLevel: Int = 0,
         viewController: UIViewController.Type? = nil,
         tasgGroupKey: TaskEnum = .personalSelection,
         subitems: [OutlineItem] = []) {
        
        self.title = title
        self.titleImage = titleImage
        self.indentLevel = indentLevel
        self.subitems = subitems
        self.outlineViewController = viewController
        self.tasgGroupKey = tasgGroupKey
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(identifier)
    }
    
    static func == (lhs: OutlineItem, rhs: OutlineItem) -> Bool {
        return lhs.identifier == rhs.identifier
    }
    
    var isGroup: Bool {
        return self.outlineViewController == nil
    }
    
    private let identifier = UUID()
}
