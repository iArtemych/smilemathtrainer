//
//  TaskEnum.swift
//  SmileMathTrainer
//
//  Created by Artem Chursin on 26.04.2020.
//  Copyright © 2020 Artem Chursin. All rights reserved.
//

import Foundation

enum TaskEnum {
    
    case personalSelection
    case agebraEGE
    case geometryEGE
    case probabilityTheoryEGE
    
    case agebraOGE
    case geometryOGE
    case probabilityTheoryOGE
    
//    case personalSelection
//
//    //algebra
//    case simpleTasks
//    case probabilityTheory
//    case appliedTasks
//    case equationЕask
//
//    //geometry
//    case squares
//    case lengths
//    case volumes
//    case onSquareGrid
}

