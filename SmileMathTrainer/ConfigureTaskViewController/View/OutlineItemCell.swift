//
//  OutlineItemCell.swift
//  SmileMathTrainer
//
//  Created by Artem Chursin on 26.04.2020.
//  Copyright © 2020 Artem Chursin. All rights reserved.
//

import UIKit

class OutlineItemCell: UICollectionViewCell {
    
    static let reuseIdentifer = "outline-item-cell-reuse-identifier"
    let label = UILabel()
    let containerView = UIView()
    let imageView = UIImageView()

    
    lazy var titleImage = UIImageView()
    
    var indentLevel: Int = 0 {
        didSet {
            indentContraint.constant = CGFloat(15 * indentLevel)
        }
    }
    var isExpanded = false {
        didSet {
            configureChevron()
        }
    }
    var isGroup = false {
        didSet {
            configureChevron()
        }
    }
    override var isHighlighted: Bool {
        didSet {
            configureChevron()
        }
    }
    override var isSelected: Bool {
        didSet {
            configureChevron()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
//        backgroundColor = .red
//        containerView.backgroundColor = .green
        
        if UIDevice.current.userInterfaceIdiom == .phone {
            configure()
        } else {
            configureiPadCell()
        }
        
        
        
        configureChevron()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            
            titleImage.layer.cornerRadius = titleImage.frame.width / 7

        } else {
            titleImage.layer.cornerRadius = titleImage.frame.width / 5
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        
        titleImage.layer.masksToBounds = true
        if UIDevice.current.userInterfaceIdiom == .pad {
            
            titleImage.layer.cornerRadius = titleImage.frame.width / 7
        } else {
            titleImage.layer.cornerRadius = titleImage.frame.width / 5
        }
    }
    
    fileprivate var indentContraint: NSLayoutConstraint! = nil
    fileprivate let inset = CGFloat(10)
}

extension OutlineItemCell {
    func configure() {
    
        titleImage.translatesAutoresizingMaskIntoConstraints = false
               containerView.addSubview(titleImage)

               titleImage.backgroundColor = .green

               imageView.translatesAutoresizingMaskIntoConstraints = false
               containerView.addSubview(imageView)

               containerView.translatesAutoresizingMaskIntoConstraints = false
               contentView.addSubview(containerView)

               label.translatesAutoresizingMaskIntoConstraints = false
               label.font = UIFont.preferredFont(forTextStyle: .headline)
               label.adjustsFontForContentSizeCategory = true
               containerView.addSubview(label)

               indentContraint = containerView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: inset)
               NSLayoutConstraint.activate([
                   indentContraint,
                   containerView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
                   containerView.topAnchor.constraint(equalTo: contentView.topAnchor),
                   containerView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),

                   imageView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 10),
                   imageView.heightAnchor.constraint(equalToConstant: 25),
                   imageView.widthAnchor.constraint(equalToConstant: 25),
                   imageView.centerYAnchor.constraint(equalTo: containerView.centerYAnchor),

                   titleImage.leadingAnchor.constraint(equalTo: imageView.leadingAnchor, constant: 40),
                   titleImage.heightAnchor.constraint(equalToConstant: 40),
                   titleImage.widthAnchor.constraint(equalToConstant: 40),
                   titleImage.centerYAnchor.constraint(equalTo: containerView.centerYAnchor),

                   label.leadingAnchor.constraint(equalTo: titleImage.trailingAnchor, constant: 10),
                   label.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
                   label.bottomAnchor.constraint(equalTo: containerView.bottomAnchor),
                   label.topAnchor.constraint(equalTo: containerView.topAnchor)
               ])
    }
    
    //TODO: - dodo
    func configureiPadCell() {
        
        titleImage.translatesAutoresizingMaskIntoConstraints = false
        containerView.addSubview(titleImage)
        
        titleImage.backgroundColor = .green
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        containerView.addSubview(imageView)
        
        containerView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(containerView)
        
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.preferredFont(forTextStyle: .headline)
        label.adjustsFontForContentSizeCategory = true
        containerView.addSubview(label)
        
        indentContraint = containerView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: inset)
        NSLayoutConstraint.activate([
            indentContraint,
            containerView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            containerView.topAnchor.constraint(equalTo: contentView.topAnchor),
            containerView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            
            imageView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 10),
            imageView.heightAnchor.constraint(equalToConstant: 35),
            imageView.widthAnchor.constraint(equalToConstant: 35),
            imageView.centerYAnchor.constraint(equalTo: containerView.centerYAnchor),
            
            titleImage.leadingAnchor.constraint(equalTo: imageView.leadingAnchor, constant: 40),
            titleImage.heightAnchor.constraint(equalToConstant: 180),
            titleImage.widthAnchor.constraint(equalToConstant: 180),
            titleImage.centerYAnchor.constraint(equalTo: containerView.centerYAnchor),
            
            label.leadingAnchor.constraint(equalTo: titleImage.trailingAnchor, constant: 10),
            label.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            label.bottomAnchor.constraint(equalTo: containerView.bottomAnchor),
            label.topAnchor.constraint(equalTo: containerView.topAnchor)
        ])
    }
    
    func configureChevron() {
        let rtl = effectiveUserInterfaceLayoutDirection == .rightToLeft
        let chevron = rtl ? "chevron.left.circle.fill" : "chevron.right.circle.fill"
        let chevronSelected = rtl ? "chevron.left.circle.fill" : "chevron.right.circle.fill"
        let circle = "circle.fill"
        let circleFill = "circle.fill"
        let highlighted = isHighlighted || isSelected
        
        if isGroup {
            let imageName = highlighted ? chevronSelected : chevron
            let image = UIImage(systemName: imageName)
            imageView.image = image
            let rtlMultiplier = rtl ? CGFloat(-1.0) : CGFloat(1.0)
            let rotationTransform = isExpanded ?
                CGAffineTransform(rotationAngle: rtlMultiplier * CGFloat.pi / 2) :
                CGAffineTransform.identity
            imageView.transform = rotationTransform
        } else {
            let imageName = highlighted ? circleFill : circle
            let image = UIImage(systemName: imageName)
            imageView.image = image
            imageView.transform = CGAffineTransform.identity
        }
        
        imageView.tintColor = highlighted ? .gray : #colorLiteral(red: 0, green: 1, blue: 0.635596633, alpha: 1)
        
    }
}

