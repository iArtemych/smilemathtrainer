//
//  TaskTypeViewController.swift
//  SmileMathTrainer
//
//  Created by Artem Chursin on 26.04.2020.
//  Copyright © 2020 Artem Chursin. All rights reserved.
//

import UIKit

class TaskTypeViewController: UIViewController {

    //MARK: - Constants
    static let titleElementKind = "title-element-kind"
    
    //MARK: - Variables
    var collectionView: UICollectionView! = nil
    var algebraController = TaskTypeController(taskKey: .personalSelection)
    var dataSource: UICollectionViewDiffableDataSource<TaskTypeController.AlgebraTaskCollection, TaskTypeController.AlgebraTask>! = nil
    var curentSnapshot: NSDiffableDataSourceSnapshot<TaskTypeController.AlgebraTaskCollection, TaskTypeController.AlgebraTask>! = nil
    var taskKey: TaskEnum = .personalSelection
    
    lazy var backgroundView: BackgroundBlureView = {
        
        let view = BackgroundBlureView()
        view.addBackgroundImage(imageName: "Background")
        return view
    }()
    
    //MARK: - LifeStyle ViewController
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = "Задачи по алгебре"
        
        setupBackground()
        configureHierarchy()
//        configureDataSource()
        updateData(taskKey: taskKey)
    }
    
    func updateData(taskKey: TaskEnum) {
        
        algebraController.updateData(taskKey: taskKey)
        configureDataSource()
    }
    
    //MARK: - Private methods
    private func setupBackground() {
        
        backgroundView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(backgroundView)
        
        backgroundView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        backgroundView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        backgroundView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        backgroundView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
    }
}

extension TaskTypeViewController {
    func createLayout() -> UICollectionViewLayout {
        
        let sectionProvider = { (sectionIndex: Int, layoutEnvironment: NSCollectionLayoutEnvironment) -> NSCollectionLayoutSection? in
            
            let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalHeight(1.0))
            let item = NSCollectionLayoutItem(layoutSize: itemSize)
            
            let groupFractionalWith = CGFloat(layoutEnvironment.container.effectiveContentSize.width > 500 ? 0.425 : 0.85)
            let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(groupFractionalWith), heightDimension: .absolute(250))
            let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitems: [item])
            
            let section = NSCollectionLayoutSection(group: group)
            
            section.orthogonalScrollingBehavior = .groupPaging
            section.interGroupSpacing = 20
            section.contentInsets = NSDirectionalEdgeInsets(top: 0, leading: 20, bottom: 0, trailing: 20)
            
            let titleSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .estimated(44))
            let titleSupplementary = NSCollectionLayoutBoundarySupplementaryItem(layoutSize: titleSize, elementKind: TaskTypeViewController.titleElementKind, alignment: .top)
            section.boundarySupplementaryItems = [titleSupplementary]
            
            return section
        }
        
        let config = UICollectionViewCompositionalLayoutConfiguration()
        config.interSectionSpacing = 20
        
        let layout = UICollectionViewCompositionalLayout(sectionProvider: sectionProvider, configuration: config)
        
        return layout
    }
}

extension TaskTypeViewController {
    func configureHierarchy() {
        
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: createLayout())
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.delegate = self
        collectionView.backgroundColor = .clear
        backgroundView.addSubview(collectionView)
        NSLayoutConstraint.activate([
            collectionView.leadingAnchor.constraint(equalTo: backgroundView.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: backgroundView.trailingAnchor),
            collectionView.topAnchor.constraint(equalTo: backgroundView.topAnchor),
            collectionView.bottomAnchor.constraint(equalTo: backgroundView.bottomAnchor)
        ])
        collectionView.register(TaskTypeCell.self, forCellWithReuseIdentifier: TaskTypeCell.reuseIdentifier)
        collectionView.register(TitleSupplementaryView.self, forSupplementaryViewOfKind: TaskTypeViewController.titleElementKind, withReuseIdentifier: TitleSupplementaryView.reuseIdentifier)
    }
    
    func configureDataSource() {
        dataSource = UICollectionViewDiffableDataSource<TaskTypeController.AlgebraTaskCollection, TaskTypeController.AlgebraTask>(collectionView: collectionView) {
            (collectionView: UICollectionView, indexPath: IndexPath, image: TaskTypeController.AlgebraTask) -> UICollectionViewCell? in

            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TaskTypeCell.reuseIdentifier, for: indexPath) as? TaskTypeCell else {
                fatalError("Cannot create new cell")
            }
            cell.titleLabel.text = image.title
            cell.categoryLabel.text = image.category
            cell.imageView.image = image.image

            return cell
        }
        dataSource.supplementaryViewProvider = { [weak self]
            (collectionView: UICollectionView, kind: String, indexPath: IndexPath) -> UICollectionReusableView? in
            guard let self = self, let snapshot = self.curentSnapshot else {
                return nil
            }

            if let titleSupplementary = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: TitleSupplementaryView.reuseIdentifier, for: indexPath) as? TitleSupplementaryView {

                let imageCategory = snapshot.sectionIdentifiers[indexPath.section]
                titleSupplementary.label.text = imageCategory.title

                return titleSupplementary
            } else {
                fatalError("Cannot create new supplementary")
            }
        }

        curentSnapshot = NSDiffableDataSourceSnapshot<TaskTypeController.AlgebraTaskCollection, TaskTypeController.AlgebraTask>()
        algebraController.collections.forEach {

            let collection = $0
            curentSnapshot.appendSections([collection])
            curentSnapshot.appendItems(collection.images)
        }
        dataSource.apply(curentSnapshot, animatingDifferences: false)
    }
    
}

extension TaskTypeViewController : UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let vc = MathMainViewController()
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true)
    }
}
