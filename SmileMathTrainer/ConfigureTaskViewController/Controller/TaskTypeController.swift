//
//  TaskTypeController.swift
//  SmileMathTrainer
//
//  Created by Artem Chursin on 26.04.2020.
//  Copyright © 2020 Artem Chursin. All rights reserved.
//

import UIKit

class TaskTypeController {
    
    struct AlgebraTask: Hashable {
        let title: String
        let category: String
        let image: UIImage?
        let identifier = UUID()
        func hash(into hasher: inout Hasher) {
            hasher.combine(identifier)
        }
    }
    
    struct AlgebraTaskCollection: Hashable {
        let title: String
        let images: [AlgebraTask]
        
        let identifier = UUID()
        func hash(into hasher: inout Hasher) {
            hasher.combine(identifier)
        }
    }
    
    var collections: [AlgebraTaskCollection] {
        return _collections
    }
    
    var taskKey: TaskEnum
    
    init(taskKey: TaskEnum = .personalSelection) {
        
        self.taskKey = taskKey
        generateCollections(taskKey: taskKey)
    }
    
    func updateData(taskKey: TaskEnum) {
        generateCollections(taskKey: taskKey)
    }
    
    fileprivate var _collections = [AlgebraTaskCollection]()
}

extension TaskTypeController {
    
    private func generateCollections(taskKey: TaskEnum) {
        
        
        switch taskKey {
            
        case .personalSelection:
            _collections = [
                AlgebraTaskCollection(title: "Простейшие задачи", images: [
                    AlgebraTask(title: "Задачи на проценты", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Задачи про поход в магаин", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Задачи на оплату услуг", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Задачи на что-то еще", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                ]),
                AlgebraTaskCollection(title: "Теория вероятностей", images: [
                    AlgebraTask(title: "Вероятность", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Монетка", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Кубик", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Связанные события", category: "Задачи на вычисление", image: UIImage(named: "Percent"))
                ]),
                AlgebraTaskCollection(title: "Еще какие то задачи", images: [
                    AlgebraTask(title: "Вероятность", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Монетка", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Кубик", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Связанные события", category: "Задачи на вычисление", image: UIImage(named: "Percent"))
                ]),
                AlgebraTaskCollection(title: "А эта херня чтоб место заполнить", images: [
                    AlgebraTask(title: "Вероятность", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Монетка", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Кубик", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Связанные события", category: "Задачи на вычисление", image: UIImage(named: "Percent"))
                ])
            ]
        case .agebraEGE:
            _collections = [
                AlgebraTaskCollection(title: "Простейшие задачи", images: [
                    AlgebraTask(title: "Задачи на проценты", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Задачи про поход в магаин", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Задачи на оплату услуг", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Задачи на что-то еще", category: "Задачи на вычисление", image: UIImage(named: "Percent"))
                ]),
                AlgebraTaskCollection(title: "Теория вероятностей", images: [
                    AlgebraTask(title: "Lol", category: "Lol на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Lol", category: "Lol на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Lol", category: "Lol на Lol", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Lol Lol", category: "Lol на Lol", image: UIImage(named: "Percent"))
                ]),
                AlgebraTaskCollection(title: "Еще какие то задачи", images: [
                    AlgebraTask(title: "Lol Lol", category: "Lol Lol на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Lol Lol", category: "Задачи на Lol Lol", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Lol Lol", category: "Задачи на Lol Lol", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Lol Lol события", category: "Lol Lol на вычисление", image: UIImage(named: "Percent"))
                ]),
                AlgebraTaskCollection(title: "А эта херня чтоб место заполнить", images: [
                    AlgebraTask(title: "Вероятность", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Монетка", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Кубик", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Связанные события", category: "Задачи на вычисление", image: UIImage(named: "Percent"))
                ])
            ]
        case .geometryEGE:
                        _collections = [
                AlgebraTaskCollection(title: "Простейшие задачи", images: [
                    AlgebraTask(title: "Задачи на проценты", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Задачи про поход в магаин", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Задачи на оплату услуг", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Задачи на что-то еще", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                ]),
                AlgebraTaskCollection(title: "Теория вероятностей", images: [
                    AlgebraTask(title: "Вероятность", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Монетка", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Кубик", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Связанные события", category: "Задачи на вычисление", image: UIImage(named: "Percent"))
                ]),
                AlgebraTaskCollection(title: "Еще какие то задачи", images: [
                    AlgebraTask(title: "Вероятность", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Монетка", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Кубик", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Связанные события", category: "Задачи на вычисление", image: UIImage(named: "Percent"))
                ]),
                AlgebraTaskCollection(title: "А эта херня чтоб место заполнить", images: [
                    AlgebraTask(title: "Вероятность", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Монетка", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Кубик", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Связанные события", category: "Задачи на вычисление", image: UIImage(named: "Percent"))
                ])
            ]
        case .probabilityTheoryEGE:
                        _collections = [
                AlgebraTaskCollection(title: "Простейшие задачи", images: [
                    AlgebraTask(title: "Задачи на проценты", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Задачи про поход в магаин", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Задачи на оплату услуг", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Задачи на что-то еще", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                ]),
                AlgebraTaskCollection(title: "Теория вероятностей", images: [
                    AlgebraTask(title: "Вероятность", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Монетка", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Кубик", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Связанные события", category: "Задачи на вычисление", image: UIImage(named: "Percent"))
                ]),
                AlgebraTaskCollection(title: "Еще какие то задачи", images: [
                    AlgebraTask(title: "Вероятность", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Монетка", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Кубик", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Связанные события", category: "Задачи на вычисление", image: UIImage(named: "Percent"))
                ]),
                AlgebraTaskCollection(title: "А эта херня чтоб место заполнить", images: [
                    AlgebraTask(title: "Вероятность", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Монетка", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Кубик", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Связанные события", category: "Задачи на вычисление", image: UIImage(named: "Percent"))
                ])
            ]
        case .agebraOGE:
                        _collections = [
                AlgebraTaskCollection(title: "Простейшие задачи", images: [
                    AlgebraTask(title: "Задачи на проценты", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Задачи про поход в магаин", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Задачи на оплату услуг", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Задачи на что-то еще", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                ]),
                AlgebraTaskCollection(title: "Теория вероятностей", images: [
                    AlgebraTask(title: "Вероятность", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Монетка", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Кубик", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Связанные события", category: "Задачи на вычисление", image: UIImage(named: "Percent"))
                ]),
                AlgebraTaskCollection(title: "Еще какие то задачи", images: [
                    AlgebraTask(title: "Вероятность", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Монетка", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Кубик", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Связанные события", category: "Задачи на вычисление", image: UIImage(named: "Percent"))
                ]),
                AlgebraTaskCollection(title: "А эта херня чтоб место заполнить", images: [
                    AlgebraTask(title: "Вероятность", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Монетка", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Кубик", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Связанные события", category: "Задачи на вычисление", image: UIImage(named: "Percent"))
                ])
            ]
        case .geometryOGE:
                        _collections = [
                AlgebraTaskCollection(title: "Простейшие задачи", images: [
                    AlgebraTask(title: "Задачи на проценты", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Задачи про поход в магаин", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Задачи на оплату услуг", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Задачи на что-то еще", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                ]),
                AlgebraTaskCollection(title: "Теория вероятностей", images: [
                    AlgebraTask(title: "Вероятность", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Монетка", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Кубик", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Связанные события", category: "Задачи на вычисление", image: UIImage(named: "Percent"))
                ]),
                AlgebraTaskCollection(title: "Еще какие то задачи", images: [
                    AlgebraTask(title: "Вероятность", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Монетка", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Кубик", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Связанные события", category: "Задачи на вычисление", image: UIImage(named: "Percent"))
                ]),
                AlgebraTaskCollection(title: "А эта херня чтоб место заполнить", images: [
                    AlgebraTask(title: "Вероятность", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Монетка", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Кубик", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Связанные события", category: "Задачи на вычисление", image: UIImage(named: "Percent"))
                ])
            ]
        case .probabilityTheoryOGE:
                        _collections = [
                AlgebraTaskCollection(title: "Простейшие задачи", images: [
                    AlgebraTask(title: "Задачи на проценты", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Задачи про поход в магаин", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Задачи на оплату услуг", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Задачи на что-то еще", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                ]),
                AlgebraTaskCollection(title: "Теория вероятностей", images: [
                    AlgebraTask(title: "Вероятность", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Монетка", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Кубик", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Связанные события", category: "Задачи на вычисление", image: UIImage(named: "Percent"))
                ]),
                AlgebraTaskCollection(title: "Еще какие то задачи", images: [
                    AlgebraTask(title: "Вероятность", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Монетка", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Кубик", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Связанные события", category: "Задачи на вычисление", image: UIImage(named: "Percent"))
                ]),
                AlgebraTaskCollection(title: "А эта херня чтоб место заполнить", images: [
                    AlgebraTask(title: "Вероятность", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Монетка", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Кубик", category: "Задачи на вычисление", image: UIImage(named: "Percent")),
                    AlgebraTask(title: "Связанные события", category: "Задачи на вычисление", image: UIImage(named: "Percent"))
                ])
            ]
        }
    }
}

