//
//  TaskListViewController.swift
//  SmileMathTrainer
//
//  Created by Artem Chursin on 26.04.2020.
//  Copyright © 2020 Artem Chursin. All rights reserved.
//

import UIKit

class TaskListViewController: UIViewController {
    
    //MARK: - Constants
    
    //MARK: - Variables
    lazy var backgroundView: BackgroundBlureView = {
        
        let view = BackgroundBlureView()
        view.addBackgroundImage(imageName: "Background")
        return view
    }()
    
    var dataSource: UICollectionViewDiffableDataSource<Section, OutlineItem>! = nil
    var outlineCollectionView: UICollectionView! = nil
    
    weak var delegate: TaskConfigurePresentDelegate?
    
    private lazy var menuItems: [OutlineItem] = {
        return [
            OutlineItem(title: "Персональная подборка", titleImage: "Empty", indentLevel: 0, viewController: TaskTypeViewController.self, tasgGroupKey: .personalSelection),
            OutlineItem(title: "ЕГЭ", titleImage: "EGE", indentLevel: 0, subitems: [
                OutlineItem(title: "Генерация варианта", titleImage: "Empty", indentLevel: 1, viewController: ConfigureListViewController.self),
                OutlineItem(title: "Отработка заданий", titleImage: "Empty", indentLevel: 1, subitems: [
                    OutlineItem(title: "Алгебра", titleImage: "Empty", indentLevel: 2, viewController: TaskTypeViewController.self, tasgGroupKey: .agebraEGE),
                    OutlineItem(title: "Геометрия", titleImage: "Empty", indentLevel: 2, viewController: TaskTypeViewController.self, tasgGroupKey: .geometryEGE),
                    OutlineItem(title: "Другое", titleImage: "Empty", indentLevel: 2, viewController: TaskTypeViewController.self, tasgGroupKey: .probabilityTheoryEGE)
                ])
            ]),
            OutlineItem(title: "ОГЭ", titleImage: "OGE", indentLevel: 0, subitems: [
                OutlineItem(title: "Генерация варианта", titleImage: "Empty", indentLevel: 1, viewController: ConfigureListViewController.self),
                OutlineItem(title: "Отработка заданий", titleImage: "Empty", indentLevel: 1, subitems: [
                    OutlineItem(title: "Алгебра", titleImage: "Empty", indentLevel: 2, viewController: TaskTypeViewController.self, tasgGroupKey: .agebraOGE),
                    OutlineItem(title: "Геометрия", titleImage: "Empty", indentLevel: 2, viewController: TaskTypeViewController.self, tasgGroupKey: .geometryOGE),
                    OutlineItem(title: "Другое", titleImage: "Empty", indentLevel: 2, viewController: TaskTypeViewController.self, tasgGroupKey: .probabilityTheoryOGE)
                ])
            ]),
            OutlineItem(title: "Другое", titleImage: "Empty", indentLevel: 0, viewController: TaskTypeViewController.self)
        ]
    }()
    
    //MARK: - LifeStyle ViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "Задания"
        
        setupBackground()
        configureCollectionView()
        configureDataSource()
    }
    
    
    
    //MARK: - Private methods
    private func setupBackground() {
        
        backgroundView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(backgroundView)
        
        backgroundView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        backgroundView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        backgroundView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        backgroundView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
    }
}

//MARK: - Configure Collection View
extension TaskListViewController {
    
    func configureCollectionView() {
        
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: generateLayout())
        
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        backgroundView.addSubview(collectionView)
        
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: view.topAnchor, constant: 80),
            collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])
        
        //TODO: - Check
        collectionView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        collectionView.backgroundColor = .clear
        collectionView.delegate = self
        collectionView.register(OutlineItemCell.self, forCellWithReuseIdentifier: OutlineItemCell.reuseIdentifer)
        self.outlineCollectionView = collectionView
    }
    
    func configureDataSource() {
        
        self.dataSource = UICollectionViewDiffableDataSource<Section, OutlineItem>(collectionView: outlineCollectionView, cellProvider: { (collectionView, indexPath, menuItem) -> UICollectionViewCell? in
            
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: OutlineItemCell.reuseIdentifer, for: indexPath) as? OutlineItemCell else {
                fatalError("Error with new cell")
            }
            cell.label.text = menuItem.title
            cell.titleImage.image = UIImage(named: menuItem.titleImage)
            cell.indentLevel = menuItem.indentLevel
            cell.isGroup = menuItem.isGroup
            cell.isExpanded = menuItem.isExpanded
            return cell
        })
        
        // load our initial data
        let snapshot = snapshotForCurrentState()
        dataSource.apply(snapshot, animatingDifferences: true)
    }
    
    func generateLayout() -> UICollectionViewLayout {
        
        let itemHeightDimension : NSCollectionLayoutDimension
        if UIDevice.current.userInterfaceIdiom == .phone {
            itemHeightDimension = NSCollectionLayoutDimension.absolute(50)
        } else {
            itemHeightDimension = NSCollectionLayoutDimension.absolute(200)
        }
        
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: itemHeightDimension)
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        
        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: itemHeightDimension)
        
        //TODO: - Group init
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitem: item, count: 1)
        
        let section = NSCollectionLayoutSection(group: group)
        section.contentInsets = NSDirectionalEdgeInsets(top: 0, leading: 30, bottom: 0, trailing: 30)
        section.interGroupSpacing = 20
        
        let layout = UICollectionViewCompositionalLayout(section: section)
        
        
        return layout
    }
    
    func snapshotForCurrentState() -> NSDiffableDataSourceSnapshot<Section, OutlineItem> {
        
        var snapshot = NSDiffableDataSourceSnapshot<Section, OutlineItem>()
        snapshot.appendSections([Section.main])
        
        func addItems(_ menuItem: OutlineItem) {
            snapshot.appendItems([menuItem])
            if menuItem.isExpanded {
                menuItem.subitems.forEach { addItems($0)}
            }
        }
        
        menuItems.forEach { addItems($0)}
        return snapshot
    }
    
    func updateUI() {
        let snapshot = snapshotForCurrentState()
        dataSource.apply(snapshot, animatingDifferences: true)
    }
}

//MARK: - UICollectionViewDelegate
extension TaskListViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        guard let menuItem = self.dataSource.itemIdentifier(for: indexPath) else {
            return
        }
        collectionView.deselectItem(at: indexPath, animated: true)
        
        if menuItem.isGroup {
            menuItem.isExpanded.toggle()
            
            if let cell = collectionView.cellForItem(at: indexPath) as? OutlineItemCell {
                UIView.animate(withDuration: 0.3) {
                    cell.isExpanded = menuItem.isExpanded
                    self.updateUI()
                }
            }
        } else {
            if let viewController = menuItem.outlineViewController {
                
                if UIDevice.current.userInterfaceIdiom == .pad {
                    
                    delegate?.updateData(for: menuItem.tasgGroupKey)
                    
                } else {
                    
                    let navController: UINavigationController
                    
                    if let vc = viewController.init() as? TaskTypeViewController {
                        
                        vc.taskKey = menuItem.tasgGroupKey
                        navController = UINavigationController(rootViewController: vc)
                        present(navController, animated: true)
                        
                    } else {
                        navController = UINavigationController(rootViewController: viewController.init())
                        present(navController, animated: true)
                    }
                    
                    
                    
//                    let navController = UINavigationController(rootViewController: viewController.init())
//                    present(navController, animated: true)
                }
            }
        }
    }
}

protocol TaskConfigurePresentDelegate: class {
    
    func updateData(for key: TaskEnum)
}

