//
//  ContainerConfigureViewController.swift
//  SmileMathTrainer
//
//  Created by Artem Chursin on 26.04.2020.
//  Copyright © 2020 Artem Chursin. All rights reserved.
//

import UIKit

class ContainerConfigureViewController: UIViewController {
    
    var mainViewController = TaskListViewController()
    var algebraViewController = TaskTypeViewController()
    
    lazy var backgroundView: BackgroundBlureView = {
         
         let view = BackgroundBlureView()
         view.addBackgroundImage(imageName: "Background")
         return view
     }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = "Выбор задания"
        setupBackground()
        
        addChildVC(mainViewController)
        addChildVC(algebraViewController)
        
        mainViewController.delegate = self
        
        setupControllers()
    }
    
    private func setupBackground() {
        
        backgroundView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(backgroundView)
        
        backgroundView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        backgroundView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        backgroundView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        backgroundView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
    }
    
    private func addChildVC(_ child: UIViewController) {
        addChild(child)
        view.addSubview(child.view)
        child.didMove(toParent: self)
    }
    
    private func setupControllers() {
        
        let mainView = mainViewController.view!
        let algebraView = algebraViewController.view!
        
        mainView.layer.borderWidth = 3
        mainView.layer.borderColor = UIColor.secondarySystemBackground.cgColor
        
        mainView.translatesAutoresizingMaskIntoConstraints = false
        algebraView.translatesAutoresizingMaskIntoConstraints = false
        
        mainViewController.backgroundView.removeElements()
        algebraViewController.backgroundView.removeElements()
        
        
        mainView.backgroundColor = nil
        algebraView.backgroundColor = nil
        
        NSLayoutConstraint.activate([
            
            mainView.topAnchor.constraint(equalTo: view.topAnchor),
            mainView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            mainView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            mainView.widthAnchor.constraint(equalToConstant: view.bounds.width * 0.4),
            
            algebraView.topAnchor.constraint(equalTo: view.topAnchor),
            algebraView.leadingAnchor.constraint(equalTo: mainView.trailingAnchor),
            algebraView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            algebraView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])
    }
}

extension ContainerConfigureViewController: TaskConfigurePresentDelegate {
    func updateData(for key: TaskEnum) {
        
        algebraViewController.updateData(taskKey: key)
    }
}
